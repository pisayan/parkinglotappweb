import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import {forkJoin} from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { ParkingLot } from '../entities/ParkingLot';
import { ParkingLotRequest } from '../entities/ParkingLotRequest';
import { UserParkEvent } from '../entities/UserParkEvent';
import { Spot } from '../entities/Spot';
import { ParkRequest } from '../entities/ParkRequest';
import { User } from '../entities/User';
import { UserParkHistoryItem } from '../entities/UserParkHistoryItem';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('access_token') })
};

const apiUrl = 'https://parkinglotapiapi20181210053135.azurewebsites.net/api/';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getParkingLots() : Observable<ParkingLot[]> {
    return this.http.get<ParkingLot[]>(apiUrl + 'ParkingLot/Lots')
    .pipe(
      tap(heroes => console.log('fetched lots')),
      catchError(this.handleError('getParkingLots', []))
    );
  }

  getParkingLot(id: number) : Observable<ParkingLot> {
    return this.http.get<ParkingLot>(apiUrl + 'ParkingLot/Lots/' + id).pipe(
      tap(_ => console.log('fetched lot id=${id}')),
      catchError(this.handleError<ParkingLot>('getParkingLot id=${id}'))
    );
  }

  addParkingLot(parkingLotRequest) : Observable<ParkingLot> {
    return this.http.post<ParkingLot>( apiUrl + 'ParkingLot/Lots', parkingLotRequest, httpOptions).pipe(
      tap((parkingLotRequest: ParkingLotRequest) => console.log('added lot w/ name = ${parkingLot.name}')),
      catchError(this.handleError<ParkingLotRequest>('addParkingLot'))
    );
  }

  getParkingLotCurrentUsers(id: number) : Observable<UserParkEvent[]> {
    return this.http.get<UserParkEvent[]>(apiUrl + 'ParkingLot/Lots/CurrentUsers/' + id).pipe(
      tap(_ => console.log('fetched lots users id=${id}')),
      catchError(this.handleError<UserParkEvent[]>('getParkingLotCurrentUsers id=${id}'))
    );
  }

  getParkingLotHistory(id: number) : Observable<UserParkEvent[]> {
    return this.http.get<UserParkEvent[]>(apiUrl + 'ParkingLot/Lots/UserHistory/' + id).pipe(
      tap(_ => console.log('fetched lots users id=${id}')),
      catchError(this.handleError<UserParkEvent[]>('getParkingLotUserHistory id=${id}'))
    );
  }

  getFirstAvailableSpot() : Observable<Spot> {
    return this.http.get<Spot>(apiUrl + 'ParkingLot/Lots/FirstAvailableSpot').pipe(
      tap(_ => console.log('fetched first spot')),
      catchError(this.handleError<Spot>('getFirstAvailableSPot'))
    );
  }

  getParkingLotsSpots(id: number) : Observable<Spot[]> {
    return this.http.get<Spot[]>(apiUrl + 'ParkingLot/Lots/GetSpots/' + id)
    .pipe(
      tap(heroes => console.log('get parking spots id=${id}')),
      catchError(this.handleError('getParkingLotsSpots id=${id}', []))
    );
  }

  parkUser(parkUser) : Observable<any> {
    return this.http.post<any>( apiUrl + 'Spot/Park', parkUser, httpOptions).pipe(
      tap((parkUser) => console.log('park user')),
      catchError(this.handleError<any>('parkUser'))
    );
  }

  exitUser(parkUser) : Observable<any> {
    return this.http.post<any>( apiUrl + 'Spot/Exit', parkUser, httpOptions).pipe(
      tap((parkUser) => console.log('exit user')),
      catchError(this.handleError<any>('exitUser'))
    );
  }

  requestParkingLotDetail(id: number): Observable<any[]> {
    let parkinglotdetail = this.http.get<ParkingLot>(apiUrl + 'ParkingLot/Lots/' + id).pipe(
      tap(_ => console.log('fetched lot id=${id}')),
      catchError(this.handleError<ParkingLot>('getParkingLot id=${id}'))
    );
    let parkinglotspots = this.http.get<Spot[]>(apiUrl + 'ParkingLot/Lots/GetSpots/' + id)
    .pipe(
      tap(heroes => console.log('get parking spots id=${id}')),
      catchError(this.handleError('getParkingLotsSpots id=${id}', []))
    );
    return forkJoin([parkinglotdetail, parkinglotspots]);
  }

  getUser(email: string) : Observable<User> {
    return this.http.get<User>(apiUrl + 'User/ByEmail?email=' + email).pipe(
      tap(_ => console.log('fetched user email=${email}')),
      catchError(this.handleError<User>('user email=${email}'))
    );
  }

  saveUser(usersave) :  Observable<any> {
    return this.http.post<any>( apiUrl + 'User/add', usersave, httpOptions).pipe(
      tap(_ => console.log('saved user')),
      catchError(this.handleError<User>('save user failed'))
    )
  }

  getUsersCurrentLot(id: number) : Observable<any> {
    return this.http.get<ParkingLot>(apiUrl + 'User/GetCurrentParkingLot/' + id).pipe(
      tap(_ => console.log('fetched lot for user id=${id}')),
      catchError(this.handleError<ParkingLot>('GetCurrentParkingLot id=${id}'))
    );
  }

  getUsersCurrentSpot(id: number) : Observable<any> {
    return this.http.get<Spot>(apiUrl + 'User/GetCurrentSpot/' + id).pipe(
      tap(_ => console.log('fetched lot for user id=${id}')),
      catchError(this.handleError<Spot>('getUsersCurrentSpot id=${id}'))
    );
  }

  getUsersParkingHistory(id: number) : Observable<UserParkHistoryItem[]> {
    return this.http.get<UserParkHistoryItem[]>(apiUrl + 'User/GetUserHistory/' + id).pipe(
      tap(_ => console.log('fetched users history id=${id}')),
      catchError(this.handleError<UserParkHistoryItem[]>('GetUserHistory id=${id}'))
    );
  }
}
