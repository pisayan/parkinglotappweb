import { Component, OnInit } from '@angular/core';
import { OAuthService, OAuthErrorEvent } from 'angular-oauth2-oidc'; // Add this import
import { ApiService } from '../../core/http/api.service';
import { UserRequest } from '../../core/entities/UserRequest';
import { User } from '../../core/entities/User';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'ParkingLot App';

  username = '';
  email = '';
  user : User;

  get token() { return this.oauthService.getAccessToken(); }
  get claims() { return this.oauthService.getIdentityClaims(); }

  constructor(private oauthService: OAuthService, private apiService: ApiService) { 
       // For debugging:
       oauthService.events.subscribe(e => e instanceof OAuthErrorEvent ? console.error(e) : console.warn(e));

       // Load information from Auth0 (could also be configured manually)
       oauthService.loadDiscoveryDocument()
   
         // See if the hash fragment contains tokens (when user got redirected back)
         .then(() => oauthService.tryLogin())
   
         // If we're still not logged in yet, try with a silent refresh:
         .then(() => {
           if (!oauthService.hasValidAccessToken()) {
             return oauthService.silentRefresh();
           }
         })
   
         // Get username, if possible.
         .then(() => {
           if (oauthService.getIdentityClaims()) {
             this.username = oauthService.getIdentityClaims()['name'];
             this.email = oauthService.getIdentityClaims()['email'];
             console.log(this.email);
             console.log(this.username);
             this.apiService.getUser(this.email)
             .subscribe(res => {
               this.user = res;
               if (this.user == null)
               {
                 this.registerCurrentUser();
               } else {
                 localStorage.setItem('userId', this.user.userId.toString())
               }
               
             }, err => {
               console.log(err);
             });
           }
         });
   
       oauthService.setupAutomaticSilentRefresh();
  }

  login() { 
    this.oauthService.initImplicitFlow(); 
  }
  logout() { this.oauthService.logOut(); }
  refresh() { this.oauthService.silentRefresh(); }
  registerCurrentUser()
  {
    const newUserRequest = new UserRequest;
    console.log(this.email);
    newUserRequest.email = this.email;
    newUserRequest.firstName = this.username;
    
    this.apiService.saveUser(newUserRequest)
    .subscribe(res => {
      console.log("Successfully saved user")
    }, err => {
      console.log(err);
    });
  }
  ngOnInit() {
  }
}