import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from '../../core/http/api.service';
import { ParkingLot } from '../../core/entities/ParkingLot';
import { Spot } from '../../core/entities/Spot';
import { ParkRequest } from '../../core/entities/ParkRequest';
import { UserParkHistoryItem } from '../entities/UserParkHistoryItem';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['name', 'address', 'lotCapacity'];
  lot: ParkingLot;
  spot: Spot;
  historyItems: UserParkHistoryItem[] = [];
  isLoadingResults = true;
  isLoadingHistory = true;
  parkedSomewhere = false;
  constructor(private api: ApiService, private _ref: ChangeDetectorRef) { 
    
  }

  getUsersCurrentLocation()
  {
    this.api.getUsersCurrentLot(parseInt(localStorage.getItem('userId')))
    .subscribe(res => {
      this.lot = res;
      if (this.lot != null && this.lot != undefined)
      {
        this.parkedSomewhere = true;
      }
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });

    this.api.getUsersCurrentSpot(parseInt(localStorage.getItem('userId')))
    .subscribe(res => {
      this.spot = res;
      if (this.spot != null && this.spot != undefined)
      {
        this.parkedSomewhere = true;
      }
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  };

  getUsersHistory()
  {
    this.isLoadingHistory = true;
    this.api.getUsersParkingHistory(parseInt(localStorage.getItem('userId')))
    .subscribe(res => {
      this.historyItems = res;
      console.log("Retrieved users history");
      console.log(this.historyItems);
      this.isLoadingHistory = false;
    }, err => {
      console.log(err);
      this.isLoadingHistory = false;
    }
    );
  };


  ngOnInit() {
    this.getUsersCurrentLocation();
    this.getUsersHistory();
  }

}
