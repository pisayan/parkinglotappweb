import { User } from './User';

export class Spot {
    spotId: number;
    spotNumber: number;
    parkingLotId: number;
    user: User;
    handicap: boolean;
    parkingRatePerHour: number;
  }