export class UserParkEvent {
    userId: number;
    firstName: string;
    lastName: string;
    email: string;
    spotId: number;
    parkingEnteredTime: string;
    parkingExitedTime: string;
    currentlyParked: boolean;
  }