export class ParkingLotRequest {
    id: number;
    name: string;
    address: string;
    city: string;
    state: string;
    zip: number;
    lotCapacity: number;
    handicapCapacity: number;
    parkingRatePerHour: number;
  }