export class UserRequest {
    firstName: string;
    lastName: string;
    email: string;
}