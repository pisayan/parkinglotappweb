export class ParkingLot {
    id: number;
    name: string;
    address: string;
    city: string;
    state: string;
    zip: number;
    lotCapacity: number;
  }