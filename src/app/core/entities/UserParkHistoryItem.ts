export class UserParkHistoryItem {
    firstName: string;
    lastName: string;
    email: string;
    parkingLotName: string;
    parkingLotAddress: string;
    parkingLotCity: string;
    parkingLotState: string;
    parkingLotZip: string;
    spotNumber: number;
    parkingEnteredTime: string;
    parkingExitedTime: string;
}