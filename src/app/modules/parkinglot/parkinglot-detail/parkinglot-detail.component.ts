import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../core/http/api.service';
import { ParkingLot } from '../../../core/entities/ParkingLot';
import { Spot } from '../../../core/entities/Spot';
import { User } from '../../../core/entities/User';
import { ParkRequest } from '../../../core/entities/ParkRequest';
import { componentRefresh } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-parkinglot-detail',
  templateUrl: './parkinglot-detail.component.html',
  styleUrls: ['./parkinglot-detail.component.scss']
})
export class ParkinglotDetailComponent implements OnInit {
  lot: ParkingLot = {
    id: null, 
    name: '', 
    address: '', 
    city: '', 
    state: '', 
    zip: null, 
    lotCapacity: null,
  };

  spotsData: Spot[] = [];

  displayedColumns: string[] = ['Number', 'Current User', 'Handicap', 'Rate'];

  userId = localStorage.getItem('userId');
  userLot: ParkingLot;
  userSpot: Spot;
  isLoadingResults = true;
  parkedSomewhere = false;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  getLotDetails(id) {
    this.api.requestParkingLotDetail(id)
    .subscribe(data => {
      this.spotsData = data[1];
      this.lot = data[0];
      console.log(this.lot);
      console.log(this.spotsData);
      this.isLoadingResults = false;
    })
  }

  parkUser(spotId) {
    this.isLoadingResults = true;
    const request = new ParkRequest;
    request.spotId = spotId;
    request.userId = parseInt(localStorage.getItem('userId'));
    this.api.parkUser(request).subscribe(data => {
      console.log("Parked User");
      this.ngOnInit();
    }, err => {
        console.log(err)
    });
    
  };

  exitUser(spotId) {
    this.isLoadingResults = true;
    const request = new ParkRequest;
    request.spotId = spotId;
    request.userId = parseInt(localStorage.getItem('userId'));
    this.api.exitUser(request).subscribe(data => {
      console.log("Exited User");
      this.parkedSomewhere = false;
      this.ngOnInit();

    }, err => {
        console.log(err)
    });
  };

  getUsersCurrentLocation()
  {
    this.api.getUsersCurrentLot(parseInt(localStorage.getItem('userId')))
    .subscribe(res => {
      this.userLot = res;
      if (this.userLot != null && this.userLot != undefined)
      {
        this.parkedSomewhere = true;
      }
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });

    this.api.getUsersCurrentSpot(parseInt(localStorage.getItem('userId')))
    .subscribe(res => {
      this.userSpot = res;
      if (this.userSpot != null && this.userSpot != undefined)
      {
        this.parkedSomewhere = true;
      }
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  };

  ngOnInit() {
    this.getLotDetails(this.route.snapshot.params['id']);
    this.getUsersCurrentLocation();
  }

}
