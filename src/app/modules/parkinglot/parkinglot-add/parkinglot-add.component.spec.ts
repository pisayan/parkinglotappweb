import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkinglotAddComponent } from './parkinglot-add.component';

describe('ParkinglotAddComponent', () => {
  let component: ParkinglotAddComponent;
  let fixture: ComponentFixture<ParkinglotAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkinglotAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkinglotAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
