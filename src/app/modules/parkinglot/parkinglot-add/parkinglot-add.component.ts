import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../core/http/api.service';
import { ParkingLotRequest } from '../../../core/entities/ParkingLotRequest';
import { ParkingLot } from '../../../core/entities/ParkingLot';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-parkinglot-add',
  templateUrl: './parkinglot-add.component.html',
  styleUrls: ['./parkinglot-add.component.scss']
})

export class ParkinglotAddComponent implements OnInit {

  parkinglotForm: FormGroup;
  name:string='';
  address:string='';
  city:string='';
  state:string='';
  zip:number=null;
  lotCapacity:number=null;
  handicapCapacity:number=null;
  parkingRatePerHour:number=null;
  isLoadingResults = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.parkinglotForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'address' : [null, Validators.required],
      'city' : [null, Validators.required],
      'state' : [null, Validators.required],
      'zip' : [null, Validators.required],
      'lotCapacity' : [null, Validators.required],
      'handicapCapacity' : [null, Validators.required],
      'parkingRatePerHour' : [null, Validators.required]
    });
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    this.api.addParkingLot(form)
      .subscribe(res => {
          let id = res['id'];
          this.isLoadingResults = false;
          this.router.navigate(['/lot', id]);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        });
  }
}
