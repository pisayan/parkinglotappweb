import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../core/http/api.service';
import { ParkingLot } from '../../../core/entities/ParkingLot';

@Component({
  selector: 'app-parkinglots',
  templateUrl: './parkinglots.component.html',
  styleUrls: ['./parkinglots.component.scss']
})

export class ParkinglotsComponent implements OnInit {
  displayedColumns: string[] = ['name', 'address', 'lotCapacity'];
  data: ParkingLot[] = [];
  isLoadingResults = true;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getParkingLots()
    .subscribe(res => {
      this.data = res;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
}
