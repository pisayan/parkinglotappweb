import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParkinglotsComponent } from './modules/parkinglot/parkinglots/parkinglots.component';
import { ParkinglotDetailComponent } from './modules/parkinglot/parkinglot-detail/parkinglot-detail.component';
import { ParkinglotAddComponent } from './modules/parkinglot/parkinglot-add/parkinglot-add.component';
import { HomeComponent } from './core/home/home.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    data: {title: 'Parking Lot App'}
  },
  {
    path: "lots",
    component: ParkinglotsComponent,
    data : {title: 'List of Parking Lots'}
  },
  {
    path: 'addlot',
    component: ParkinglotAddComponent,
    data: { title: 'Add Parking Lot' }
  },
  {
    path: "lot/:id",
    component: ParkinglotDetailComponent,
    data : {title: 'Parking Lot Detail'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
